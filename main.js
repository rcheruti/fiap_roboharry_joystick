

var http = require('http');
var express = require('express');
var bodyParser = require('body-parser');
var Gpio;

try{
  Gpio = require('pigpio').Gpio;
}catch(ex){
  console.log('Sem PIGPIO');
}

function _exitF(opts, err){
  if(Gpio){
    m1GPIO = null;
    m2GPIO = null;
    m3GPIO = null;
    m4GPIO = null;
  }
  if(err){
    console.log(err.stack);
  }
  console.log('Tipo de saída: ', opts.type);
  if(opts.exit) process.exit();
}
process.on('exit', _exitF.bind(null,{exit: true, type:'exit'}));
process.on('SIGINT', _exitF.bind(null,{exit: true, type:'SIGINT'}));
process.on('uncaughtException', _exitF.bind(null,{exit: true, type:'uncaughtException'}));

var expressApp = express();

var server = http.createServer(expressApp);

server.listen(9000, '127.0.0.1');

expressApp.use('/joy', express.static('public'));
expressApp.use('/', bodyParser.json());


var maxVel = 255;
var m1GPIO, m2GPIO, m3GPIO, m4GPIO;
if( Gpio ){
  m1GPIO = new Gpio(12, {mode: Gpio.OUTPUT});
  m2GPIO = new Gpio(13, {mode: Gpio.OUTPUT});
  m3GPIO = new Gpio(16, {mode: Gpio.OUTPUT});
  m4GPIO = new Gpio(19, {mode: Gpio.OUTPUT});
}

// body: { m1: [0.0 ~ 1.0], m2: ... }
expressApp.all('/', function(req,res,next){
  if(!req.body){
    res.send({status: false});
    return;
  }
  var v = 0, m1 = 0, m2 = 0, m3 = 0, m4 = 0;
  if( typeof(req.body.m1) === 'number' ){
    var v = Math.abs(req.body.m1);
    m1 = Math.round( v * maxVel);
    if( req.body.m1 < 0 ){
      m3 = m1;
      m1 = 0;
    }
  }
  if( typeof(req.body.m2) === 'number' ){
    var v = Math.abs(req.body.m2);
    m2 = Math.round( v * maxVel);
    if( req.body.m2 < 0 ){
      m4 = m2;
      m2 = 0;
    }
  }
  
  if(Gpio){
    m1GPIO.pwmWrite( m1 );
    m2GPIO.pwmWrite( m2 );
    m3GPIO.pwmWrite( m3 );
    m4GPIO.pwmWrite( m4 );
  }
  var x1 = (req.body.m1 < 0? -m3 : m1) / maxVel,
      x2 = (req.body.m2 < 0? -m4 : m2) / maxVel;
  res.send({ status: true, data:{ m1: x1, m2: x2 } });
});

console.log('---  Serv iniciado');
