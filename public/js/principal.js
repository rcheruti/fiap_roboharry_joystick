
$(function(){
  
  var joystick = $('.joystick');
  var circulo = joystick.find('.circulo');
  
  var mc = new Hammer.Manager( circulo[0] );
  mc.add( new Hammer.Pan({ direction: Hammer.DIRECTION_ALL, threshold: 0 }) );
  
  var maxRaio = (joystick[0].offsetWidth / 2) 
              - (circulo[0].offsetWidth / 2) ;
  
  //console.log(maxRaio);
  
  mc.on('panstart', function(ev){
    
  });
  mc.on('panmove', function(ev){
    _set(ev.deltaX, ev.deltaY, ev);
  });
  mc.on('panend', function(ev){
    _set(0,0);
  });
  
  var enviar = null;
  var lastMsg = null;
  var st = setInterval(function(){
    if( lastMsg !== enviar ){
      $.post('/', enviar);
      lastMsg = enviar;
    }
  }, 30);
  
  function _set(x,y, ev){
    x = x||0;
    y = y||0;
    
    if(ev){
      
      if( ev.distance > maxRaio ){
        x = (x / ev.distance) * maxRaio;
        y = (y / ev.distance) * maxRaio;
      }
      var m1, m2, lvl = (ev.distance < maxRaio? ev.distance : maxRaio);
      
      if( ev.angle < 0 && ev.angle > -90 ){ // dir top
        m1 = lvl / maxRaio;
        m2 = ((Math.abs(ev.angle)) / 90) * m1;
      }else if( ev.angle < -90 && ev.angle > -180 ){ // esq top
        m2 = lvl / maxRaio;
        m1 = ( (ev.angle +180) / 90) * m2;
      }else if( ev.angle > 0 && ev.angle < 90 ){ // dir bot
        m1 = lvl / maxRaio * -1;
        m2 = ((Math.abs(ev.angle)) / 90) * m1;
      }else if( ev.angle > 90 && ev.angle < 180 ){ // esq bot
        m2 = lvl / maxRaio * -1;
        m1 = (-1* (ev.angle -180) / 90) * m2;
      }
      enviar = {
        m1: m1,
        m2: m2
      };
    }else{
      enviar = {
        m1: 0,
        m2: 0
      };
    }
    circulo[0].style.transform = 
      ' translate3d( calc(-50% + '+ x +'px) , calc(-50% + '+ y +'px) , 0 ) ';
  }
  
});
